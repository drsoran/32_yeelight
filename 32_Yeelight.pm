package main;

use strict;
use warnings;

use IO::Socket::INET;
use JSON;
use Data::Dumper;
use Scalar::Util;
use Color;


my %Yeelight_Sets = (
    on => "on",
    off => "off",
    toggle => "toggle",
    rgb => "rgb",
    bright => "bright",
    ct => "ct",
    hsv => "hsv",
    hue => "hue",
    sat => "sat",
    color => "color",
    connect => "connect",
    disconnect => "disconnect"
);

my %Yeelight_Props = (
    state => "power",
    bright => "bright",
    ct => "ct",
    rgb => "rgb",
    hue => "hue",
    sat => "sat",
    color_mode => "color_mode",
    flowing => "flowing",
    flow_params => "flow_params",
    delayoff => "delayoff",
    music_on => "music_on",
    name => "name"
);

my %Yeelight_Gets = (
    state => sub { return ReadingsVal(shift->{NAME}, "state", "off"); },
    dim   => sub { return ReadingsVal(shift->{NAME}, "bright", 0); },
    rgb   => sub { return ReadingsVal(shift->{NAME}, "rgb", 0); },
    ct    => sub { return ReadingsVal(shift->{NAME}, "ct", 0); },
    hsv   => sub {
        my $name = shift->{NAME};
        return ReadingsVal($name, "hue", 0) . "," . ReadingsVal($name, "sat", 0) . "," . ReadingsVal($name, "bright", 1);
    }
);

my %Yeelight_ColorMode = (
    rgb => 1,
    ct  => 2,
    hsv => 3
);

my %Yeelight_ReadingsConverter = (
                  # yee2fhem
    rgb        => sub { return sprintf("%06X", shift); },
    color_mode => sub {
                          my $m = shift;
                          my %h = reverse %Yeelight_ColorMode;
                          return $h{$m};
                      }
);

sub Yeelight_Initialize($)
{
  my ($hash) = @_;

  $hash->{DefFn}    = "Yeelight_Define";
  $hash->{UndefFn}  = "Yeelight_Undefine";
  $hash->{NotifyFn} = "Yeelight_Notify";
  $hash->{ReadFn}   = "Yeelight_Read";
  $hash->{SetFn}    = "Yeelight_Set";
  $hash->{GetFn}    = "Yeelight_Get";
  $hash->{AttrFn}   = "Yeelight_Attr";
  $hash->{AttrList} =
    "disable:1 defaultFade $readingFnAttributes";

  return undef;
}

#####################################

sub Yeelight_Define($$)
{
    my ($hash, $def) = @_;

    my @args = split("[ \t][ \t]*", $def);
    my $argsLen = @args;

    return "Usage: define <name> Yeelight <ip|host>"  if ($argsLen < 3);

    my $name = $args[0];
    my $host = $args[2];

    my $definition = $host;

    $hash->{DEF} = $definition;
    $hash->{NOTIFYDEV} = "global";
    $hash->{STATE} = "initialized";
    $hash->{NAME} = $name;

    $hash->{helper}{host} = $host;
    $hash->{helper}{JSON} = JSON->new->utf8();
    $hash->{helper}{nextMsgId} = 1;
    $hash->{helper}{msgHandlerMap} = {};

    return undef;
}

sub Yeelight_Undefine($$)
{
    my ($hash, $arg) = @_;

    RemoveInternalTimer($hash);
    Yeelight_disconnect($hash);

    return undef;
}

sub Yeelight_Set($$@)
{
    my ($hash, $name, $cmd, @params) = @_;

    if (!defined $cmd || !defined($Yeelight_Sets{$cmd}))
    {
        return "Unknown argument, choose one of " . join(" ", values %Yeelight_Sets);
    }

    $cmd = lc $cmd;

    if ($cmd eq "connect")
    {
        Yeelight_connect($hash);
    }
    elsif ($cmd eq "disconnect")
    {
        Yeelight_disconnect($hash);
    }
    elsif ($cmd eq "on" || $cmd eq "off")
    {
        return Yeelight_set_power($hash, $cmd, @params);
    }
    elsif ($cmd eq "toggle")
    {
        return Yeelight_toggle($hash);
    }
    elsif ($cmd eq "rgb")
    {
        return Yeelight_set_rgb($hash, @params);
    }
    elsif ($cmd eq "bright")
    {
        return Yeelight_set_bright($hash, @params);
    }
    elsif ($cmd eq "ct")
    {
        return Yeelight_set_ct_abx($hash, @params);
    }
    elsif ($cmd eq "hsv")
    {
        return Yeelight_set_hsv($hash, @params);
    }
    elsif ($cmd eq "hue")
    {
        return Yeelight_set_hue($hash, @params);
    }
    elsif ($cmd eq "sat")
    {
        return Yeelight_set_sat($hash, @params);
    }
    elsif ($cmd eq "color")
    {
        return Yeelight_set_color($hash, @params);
    }

    return undef;
}

sub Yeelight_Get($$@)
{
    my ($hash, $name, $opt, @params) = @_;

    if(!defined($Yeelight_Gets{$opt}))
    {
        return "Unknown argument $opt, choose one of " . join(" ", keys %Yeelight_Gets);
    }
    
    my $getter = $Yeelight_Gets{$opt};
    $getter->($hash);
}

sub Yeelight_Notify($$)
{
    my ($hash, $dev) = @_;

    return if($dev->{NAME} ne "global");
    return if(!grep(m/^INITIALIZED|REREADCFG$/, @{$dev->{CHANGED}}));

    Yeelight_reconnect($hash);

    return undef;
}

sub Yeelight_Read($)
{
    my ($hash) = @_;
    my $name = $hash->{NAME};

    my $buf = "";
    my $ret = sysread($hash->{CD}, $buf, 4096);

    if (!defined($ret) || $ret <= 0)
    {
        Yeelight_disconnect($hash);

        InternalTimer(gettimeofday() + 30, "Yeelight_reconnect", $hash, 0);
        return;
    }

    my $json = $hash->{helper}{JSON}->incr_parse($buf);
    if ($json)
    {
        Log3 $name, 5, "$name: Read:\n" . Dumper(\$json);
        $hash->{helper}{JSON}->incr_reset();

        Yeelight_handleMessage($hash, $json);
    }
}

sub Yeelight_Attr($$$)
{
    my ($cmd, $name, $attrName, $attrVal) = @_;

    if ($attrName eq "disable")
    {
        my $hash = $defs{$name};
        RemoveInternalTimer($hash);

        if ($cmd eq "set" && $attrVal ne "0")
        {
            $attrVal = 1;
            Yeelight_disconnect($hash);
        }
        else
        {
            $attr{$name}{$attrName} = 0;
            Yeelight_connect($hash);
        }
    }
    elsif ($attrName eq "defaultFade")
    {
        return "defaultFade must be an integer" if (!looks_like_number($attrVal));
    }

    return undef;
}

#####################################

sub Yeelight_connect($)
{
    my ($hash, $retry) = @_;
    my $name = $hash->{NAME};

    return if (AttrVal($hash->{NAME}, "disable", 0));

    Yeelight_disconnect($hash);

    Log3 $name, 4, "$name: connecting";

    my $timeout = $hash->{TIMEOUT} ? $hash->{TIMEOUT} : 3;
    my $conn = IO::Socket::INET->new(PeerAddr => "$hash->{helper}{host}:55443", Timeout => $timeout);

    if ($conn)
    {
        Log3 $name, 3, "$name: connected";

        $hash->{helper}{ConnectionState} = "Connected";

        if ($hash->{helper}{ConnectionState} ne ReadingsVal($name, "state", "" ))
        {
            readingsSingleUpdate($hash, "state", $hash->{helper}{ConnectionState}, 1);
        }

        $hash->{FD} = $conn->fileno();
        $hash->{CD} = $conn;

        $selectlist{$name} = $hash;

        Yeelight_updateAllReadings($hash);

        InternalTimer(gettimeofday() + 10, "Yeelight_keepAlive", $hash, 0);
    }
    else
    {
        Log3 $name, 4, "$name: connect to $hash->{helper}{host} failed";
    }

    return undef;
}

sub Yeelight_disconnect($)
{
    my $hash = shift;
    my $name = $hash->{NAME};

    RemoveInternalTimer($hash);

    $hash->{helper}{ConnectionState} = "Disconnected";
    if ($hash->{helper}{ConnectionState} ne ReadingsVal($name, "state", "" ))
    {
        readingsSingleUpdate($hash, "state", $hash->{helper}{ConnectionState}, 1);
    }

    return if (!$hash->{CD});
    Log3 $name, 3, "$name: disconnecting";

    close($hash->{CD});
    delete($hash->{FD});
    delete($hash->{CD});

    $hash->{helper}{msgHandlerMap} = {};
    $hash->{helper}{nextMsgId} = 1;

    return undef;
}

sub Yeelight_reconnect($)
{
    my $hash = shift;
    my $name = $hash->{NAME};

    return if (AttrVal($name, "disable", 0));

    Yeelight_connect($hash);

    if ($hash->{helper}{ConnectionState} ne "Connected")
    {
        InternalTimer(gettimeofday() + 2, "Yeelight_reconnect", $hash, 0);
    }
}

sub Yeelight_keepAlive($)
{
    my $hash = shift;

    return undef if(!$hash->{CD});

    my $name = $hash->{NAME};
    Log3 $name, 4, "$name: keep alive";

    my @properties = ("power");
    my $msg = {
        method => "get_prop",
        params => \@properties
    };

    Yeelight_sendMessage($hash, $msg, undef, \@properties);
    InternalTimer(gettimeofday() + 10, "Yeelight_keepAlive", $hash, 0);
}

sub Yeelight_sendMessage($$$$)
{
    my ($hash, $msg, $callback, $args) = @_;
    my $name = $hash->{NAME};

    return undef if(!$hash->{CD});

    my $id = $hash->{helper}{nextMsgId};
    if ($hash->{helper}{nextMsgId} == 0x7FFFFFFF)
    {
        $hash->{helper}{nextMsgId} = 1;
    }
    else
    {
        ++$hash->{helper}{nextMsgId};
    }

    $msg->{id} = $id;
    my $json = $hash->{helper}{JSON}->encode($msg) . "\r\n";

    Log3 $name, 5, "$name: send:\n" . $json;

    if (defined($callback))
    {
        my $handlerObj = {};
        $handlerObj->{callback} = $callback;
        $handlerObj->{args} = $args if (defined($args));

        $hash->{helper}{msgHandlerMap}->{$id} = $handlerObj;
    }

    my $ret = syswrite($hash->{CD}, $json);
    # Log3 $name, 5, "$name: syswrite $ret";

    if (!defined($ret) || $ret <= 0)
    {
        Yeelight_disconnect($hash);
        InternalTimer(gettimeofday() + 2, "Yeelight_reconnect", $hash, 0);
    }

    return undef;
}

sub Yeelight_updateReadingsAfterSetValue($$@)
{
    my ($hash, $unused, $readings) = @_;

    my @properties = ();
    foreach (@$readings)
    {
        push @properties, $Yeelight_Props{$_};
    }

    my $msg = {
        method => "get_prop",
        params => \@properties
    };

    Yeelight_sendMessage($hash, $msg, \&Yeelight_on_get_prop, \@properties);
}

sub Yeelight_updateAllReadings($)
{
    my $hash = shift;

    my @properties = values %Yeelight_Props;

    my $msg = {
        method => "get_prop",
        params => \@properties
    };

    Yeelight_sendMessage($hash, $msg, \&Yeelight_on_get_prop, \@properties);
}

sub Yeelight_handleMessage($$)
{
    my ($hash, $msg) = @_;

    Yeelight_handleNotificationMessage($hash, $msg) if (!defined($msg->{id}));
    Yeelight_handleResultMessage($hash, $msg) if (defined($msg->{id}));
}

sub Yeelight_handleNotificationMessage($$)
{
    my ($hash, $msg) = @_;
    my $name = $hash->{NAME};

    if ($msg->{method} ne "props")
    {
        Log3 $name, 3, "$name: skip notification for unknown method $msg->{method}";
        return;
    }

    readingsBeginUpdate($hash);

    my %fhemReadings = reverse %Yeelight_Props;

    for my $property (keys %{$msg->{params}})
    {
        my $fhemReading = $fhemReadings{$property};
        if (!defined($fhemReading))
        {
            Log3 $name, 3, "$name: skip notification for unknown property $property";
            next;
        }

        my $value = Yeelight_getFhemReadingsValue($fhemReading, $msg->{params}{$property});
        readingsBulkUpdate($hash, $fhemReading, $value);
    }

    readingsEndUpdate($hash, 1);
}

sub Yeelight_handleResultMessage($$)
{
    my ($hash, $msg) = @_;
    my $name = $hash->{NAME};

    my $error = $msg->{error};
    if (defined($error))
    {
        Log3 $name, 1, "$name: error $msg->{error}{code}, $msg->{error}{message}";
        return;
    }

    my $id = $msg->{id};
    my $handler = $hash->{helper}{msgHandlerMap}->{$id};
    return if (!defined($handler));

    my $callback = $handler->{callback};
    my $args = $handler->{args};

    delete($hash->{helper}{msgHandlerMap}->{$id});

    $callback->($hash, $msg)        if (!defined($args));
    $callback->($hash, $msg, $args) if (defined($args));
}

sub Yeelight_getFhemReadingsValue($$)
{
    my ($fhemReading, $value) = @_;
    return $value if (!exists($Yeelight_ReadingsConverter{$fhemReading}));
    return $Yeelight_ReadingsConverter{$fhemReading}->($value);
}

sub Yeelight_addDuration($$\@)
{
    my $hash = shift; my $duration = shift; my $yeeParams = shift;
    my $defaultFade = AttrVal($hash->{NAME}, "defaultFade", undef);

    $duration = $defaultFade if (!defined($duration));

    if (!defined($duration) || $duration < 30)
    {
        push @$yeeParams, "sudden";
        push @$yeeParams, 0;
    }
    elsif (!looks_like_number($duration))
    {
        return "Usage: fade must be a number";
    }
    else
    {
        push @$yeeParams, "smooth";
        push @$yeeParams, $duration + 0; # force number
    }

    return undef;
}

sub Yeelight_checkNumericValue($$$$)
{
    my ($reading, $value, $min, $max) = @_;

    if (!looks_like_number($value) || $value < $min || $value > $max)
    {
        return "Usage: $reading value must be an integer between $min and $max but was $value";
    }

    return undef;
}

sub Yeelight_on_get_prop($$$)
{
    my ($hash, $msg, $properties) = @_;

    readingsBeginUpdate($hash);

    my %fhemReadings = reverse %Yeelight_Props;

    for (my $i = 0; $i <= $#$properties; $i++)
    {
        my $fhemReading = $fhemReadings{$properties->[$i]};
        my $value = Yeelight_getFhemReadingsValue($fhemReading, $msg->{result}[$i]);

        readingsBulkUpdate($hash, $fhemReading, $value);
    }

    readingsEndUpdate($hash, 1);
}

sub Yeelight_set_power($$@)
{
    my ($hash, $state, @params) = @_;
    return Yeelight_set_Value($hash, "state", "set_power", $state, $params[0]);
}

sub Yeelight_toggle($)
{
    my $hash = shift;
    my @yeeParams = ();

    my $msg = {
        method => "toggle",
        params => \@yeeParams
    };

    my @readings = ("state");
    Yeelight_sendMessage($hash, $msg, \&Yeelight_updateReadingsAfterSetValue, \@readings);

    return undef;
}

sub Yeelight_set_rgb($@)
{
    my ($hash, @params) = @_;

    $params[0] = hex $params[0]; # Yee wants RGB as integer value
    return Yeelight_set_NumericValue($hash, "rgb", "set_rgb", 0, 0xFFFFFF, @params);
}

sub Yeelight_set_bright($@)
{
    my ($hash, @params) = @_;
    return Yeelight_set_NumericValue($hash, "bright", "set_bright", 1, 100, @params);
}

sub Yeelight_set_ct_abx($@)
{
    my ($hash, @params) = @_;
    return Yeelight_set_NumericValue($hash, "ct", "set_ct_abx", 1700, 6500, @params);
}

sub Yeelight_set_hsv($@)
{
    my ($hash, @params) = @_;

    #Log3 $hash->{name}, 3, "" . join(",", @params);

    my @hsv = split(',', $params[0]);
    if (scalar(@hsv) < 3)
    {
        return "Usage: hsv <hue>,<sat>,<val>";
    }

    my $hue = trim($hsv[0]);
    my $err = Yeelight_checkNumericValue("hue", $hue, 0, 359);
    return $err if (defined($err));

    my $sat = trim($hsv[1]);
    $err = Yeelight_checkNumericValue("sat", $sat, 0, 100);
    return $err if (defined($err));

    my $val = trim($hsv[2]);
    $err = Yeelight_checkNumericValue("val", $val, 1, 100);
    return $err if (defined($err));

    my @yeeParams = ("hsv", $hue + 0, $sat + 0, $val + 0);

    my $msg = {
        method => "set_scene",
        params => \@yeeParams
    };

    my @readings = ("hue", "sat", "bright");
    Yeelight_sendMessage($hash, $msg, \&Yeelight_updateReadingsAfterSetValue, \@readings);

    return undef;
}

sub Yeelight_set_hue($@)
{
    my ($hash, @params) = @_;
    my $name = $hash->{name};

    my $sat = $hash->{READINGS}{sat}{VAL};
    my $bright = $hash->{READINGS}{bright}{VAL};
    $params[0] = "$params[0],$sat,$bright";

    return Yeelight_set_hsv($hash, @params);
}

sub Yeelight_set_sat($@)
{
    my ($hash, @params) = @_;
    my $name = $hash->{name};

    my $hue = $hash->{READINGS}{hue}{VAL};
    my $bright = $hash->{READINGS}{bright}{VAL};

    $params[0] = "$hue,$params[0],$bright";
    return Yeelight_set_hsv($hash, @params);
}

sub Yeelight_set_color($@)
{
    my ($hash, @params) = @_;
    my @rgbAndBright = split(',', $params[0]);

    if (scalar(@rgbAndBright) < 2)
    {
        return "Usage: color <rgb>,<bright>";
    }

    my $rgb = hex trim($rgbAndBright[0]);
    my $err = Yeelight_checkNumericValue("rgb", $rgb, 0, 0xFFFFFF);
    return $err if (defined($err));

    my $bright = trim($rgbAndBright[1]);
    $err = Yeelight_checkNumericValue("bright", $bright, 1, 100);
    return $err if (defined($err));

    my @yeeParams = ("color", $rgb + 0, $bright + 0);

    my $msg = {
        method => "set_scene",
        params => \@yeeParams
    };

    my @readings = ("rgb", "bright");
    Yeelight_sendMessage($hash, $msg, \&Yeelight_updateReadingsAfterSetValue, \@readings);

    return undef;
}

sub Yeelight_set_NumericValue($$$$$@)
{
    my ($hash, $reading, $method, $min, $max, @params) = @_;

    my $value = $params[0];
    my $err = Yeelight_checkNumericValue($reading, $value, $min, $max);
    return $err if (defined($err));

    $value = $value + 0; # force number
    return Yeelight_set_Value($hash, $reading, $method, $value, $params[1]);
}

sub Yeelight_set_Value($$$$$)
{
    my ($hash, $reading, $method, $value, $duration) = @_;

    my @yeeParams = ($value);

    my $err = Yeelight_addDuration($hash, $duration, @yeeParams);
    return $err if (defined($err));

    my $msg = {
        method => $method,
        params => \@yeeParams
    };

    # we always get the color_mode because it is not always updates by Yee
    my @readings = ($reading, "color_mode");
    Yeelight_sendMessage($hash, $msg, \&Yeelight_updateReadingsAfterSetValue, \@readings);

    return undef;
}

1;

=pod
=item summary    module for RGBW Yeelight LED Bulb (Color)
=item summary_DE Modul f�r die RGBW Yeelight LED Bulb (Color)
=begin html

<a name="32_Yeelight.pm"></a>
<h3>Yeelight</h3>
<ul>
  Defines a device to integrate a Yeelight LED Bulb (RGBW) into fhem.<br><br>
  It is possible to: switch the bulb, dim the bulb, change the color in RGB and HSV mode and change the color temperature. It is currently not possible to start an automatic color fade program or program a delayed off.<br><br>
  Notes:
  <ul>
    <li>JSON has to be installed on the FHEM host.</li>
    <li>The "developer mode" must be active in the bulb. It can be switched on in the bulb settings inside the Yeelight App.</li>
    <li>The module will communicate locally with the device. This means it is not needed - and you surely don't want - to grant the bulb internet access in your router.</li>
    <li>LightScene integration:<br>The bulb can operate in color RGB/HSV mode or in white color temperature mode. This is indicated by the <i>color_mode</i> reading. If you want to save a scene depending on the color_mode, you can use the lightSceneParamsToSave LightScene attribute at the device.<br><br>
    Example:<br><br>
    <code>
    define floor_lamp Yeelight 10.1.3.5<br>
    attr floor_lamp lightSceneParamsToSave {(ReadingsVal($DEVICE, "color_mode", "rgb") eq "ct") ? "ct" : "rgb"}
    </code>
    </li>
  </ul><br>
  <a name="Yeelight_Define"></a>
  <b>Define</b>
  <ul>
    <code>define &lt;name&gt; Yeelight &lt;ip or hostname&gt;</code><br>
    <br>
    Defines a Yeelight device.<br><br>
    Examples:
    <ul>
      <code>define floor_lamp Yeelight 10.0.1.4</code><br>
      <code>define kitchen_lamp Yeelight yee-kitchen_lamp</code><br>
    </ul>
  </ul><br>
  <a name="Yeelight_Readings"></a>
  <b>Readings</b>
  <ul>
    <li>state<br>
      the on/off state of the bulb.</li>
    <li>bright<br>
      the current bightness level of the bulb.</li>
    <li>color_mode<br>
      the mode the bulb currently operates in. Only for information. The possible values are:
      <ul>
          <li><i>hsv</i> the bulb operates in HSV mode.</li>
          <li><i>rbg</i> the bulb operates in RGB mode.</li>
          <li><i>ct</i> the bulb operates in color temperature (white) mode.</li>
      </ul></li>
    <li>ct<br>
      the recent set color temperature. Also available if not in color_mode ct.</li>
    <li>delayoff<br>
      the set delayed off timer. Only for information.</li>
    <li>flow_params<br>
      the set automatic color flow parameters. Only for information.</li>
    <li>flowing<br>
      1, if an automatic color flow is active, 0 otherwise. Only for information.</li>
    <li>hue<br>
      the recent set hue value. Also available if not in color_mode hsv.</li>
    <li>sat<br>
      the recent set saturation value. Also available if not in color_mode hsv.</li>
    <li>music_on<br>
      1, if the bulb operates in music mode, 0 otherwise. Only for information.</li>
    <li>name<br>
      the internal name of the bulb. Only for information.</li>
    <li>rgb<br>
      the recent set RBG value. Also available if not in color_mode rgb.</li>
  </ul><br>
  <a name="Yeelight_Set"></a>
  <b>Set</b>
  <br><br>
  Almost every set command supports an optional <i>fade</i> parameter. This will instruct the bulb to do a fade to the target value instead of an sudden change. The parameter has to be given in milliseconds. If the value is less than 30, it will be interpreted as 0 because 30 ms is the lower limit the Yeelight can fade.<br><br>
  A default fade time used in all commands can be set as attribute.
  <ul>
    <li>on [&lt;fade&gt;]<br>
      switch the bulb on and optionally fade to on for &lt;fade&gt; milliseconds.
      <br><br>
      Examples:
      <ul>
        <code>set floor_lamp on</code><br>
        <code>set floor_lamp on 1000</code><br>
      </ul>
      <br></li>
    <li>off [&lt;fade&gt;]<br>
      switch the bulb off and optionally fade to off for &lt;fade&gt; milliseconds.
      <br><br>
      Examples:
      <ul>
        <code>set floor_lamp off</code><br>
        <code>set floor_lamp off 1000</code><br>
      </ul>
      <br></li>
    <li>bright &lt;value&gt; [&lt;fade&gt;]<br>
      sets the brightness of the bulb to <i>value</i> and optionally fade to it for &lt;fade&gt; milliseconds.<br><i>value</i> has to be in range (1, 100). NOTE: The command is only effective if the bulb is on.<br><br>
      Examples:
      <ul>
        <code>set floor_lamp bright 70</code><br>
        <code>set floor_lamp bright 10 500</code><br>
      </ul>
      <br></li>
    <li>ct &lt;value&gt; [&lt;fade&gt;]<br>
      sets the color temperature [K] of the bulb to <i>value</i> and optionally fade to it for &lt;fade&gt; milliseconds.<br><i>value</i> has to be in range (1700, 6500). This will set the bulb into color_mode ct. NOTE: The command is only effective if the bulb is on.<br><br>
      Examples:
      <ul>
        <code>set floor_lamp ct 5000</code><br>
        <code>set floor_lamp ct 5000 500</code><br>
      </ul>
      <br></li>
    <li>hue &lt;value&gt;<br>
      sets the HSV hue component of the bulb to <i>value</i><br><i>value</i> has to be in range (0, 359). This will set the bulb into color_mode hsv. NOTE: The command will switch on the device.<br><br>
      Examples:
      <ul>
        <code>set floor_lamp hue 100</code><br>
      </ul>
      <br></li>
    <li>sat &lt;value&gt;<br>
      sets the HSV saturation component of the bulb to <i>value</i>.<br><i>value</i> has to be in range (0, 100). This will set the bulb into color_mode hsv. NOTE: The command will switch on the device.<br><br>
      Examples:
      <ul>
        <code>set floor_lamp sat 100</code><br>
      </ul>
      <br></li>
    <li>rgb &lt;hex value&gt; [&lt;fade&gt;]<br>
      sets the bulb color to the RGB <i>value</i> and optionally fade to it for &lt;fade&gt; milliseconds.<br><i>value</i> has to be hexadecimal in range (000000, FFFFFF). This will set the bulb into color_mode rgb. NOTE: The command is only effective if the bulb is on.<br><br>
      Examples:
      <ul>
        <code>set floor_lamp rgb AA02F2</code><br>
        <code>set floor_lamp rgb FFFFFF 500</code><br>
      </ul>
      <br></li>
    <li>color &lt;RGB value&gt,&lt;brightness&gt;<br>
      sets the bulb color to the RGB <i>value</i> and the bightness &lt;brightness&gt.<br><i>RGB value</i> has to be hexadecimal in range (000000, FFFFFF), <i>brightness</i> has to be a value in the range of (1, 100). This will set the bulb into color_mode rgb. NOTE: The command will switch on the device.<br><br>
      Examples:
      <ul>
        <code>set floor_lamp color AA02F2,30</code><br>
      </ul>
      <br></li>
    <li>hsv &lt;hue&gt,&lt;saturation&gt;,&lt;value&gt;<br>
      sets the bulb color to the HSV value.<br><i>hue</i> has to be value in range (0, 359), <i>saturation</i> has to be a value in the range of (0, 100), <i>value</i> has to be a value in the range of (1, 100). This will set the bulb into color_mode hsv. NOTE: The command will switch on the device.<br><br>
      Examples:
      <ul>
        <code>set floor_lamp hsv 120,30,50</code><br>
      </ul>
      <br></li>
    <li>toggle<br>
      toggles the on, off state of the bulb.<br><br>
      Examples:
      <ul>
        <code>set floor_lamp toggle</code><br>
      </ul>
      <br></li>
    <li>connect<br>
      reconnects to the bulb. This will refresh all readings.<br><br>
      Examples:
      <ul>
        <code>set floor_lamp connect</code><br>
      </ul>
      <br></li>
    <li>disconnect<br>
      disconnects from the bulb.<br><br>
      Examples:
      <ul>
        <code>set floor_lamp disconnect</code><br>
      </ul>
      <br></li>
  </ul>
  <a name="Yeelight_Get"></a>
  <b>Get</b>
  <ul>
    <li>hsv<br>
      get the bulb's current HSV value in the format <i>hue,sat,value</i><br><br>
      Examples:
      <ul>
        <code>get floor_lamp hsv</code> will return 120,30,10<br>
      </ul>
      </li>
  </ul>
  <a name="Yeelight_Attr_Attr"></a>
  <b>Attributes</b>
  <ul>
    <li>defaultFade &lt;value&gt;<br>
      sets a default fade value in milliseconds that will always be used in any Set command that supports a fade. This value can be overridden by passing a fade value as parameter to the individual Set command.<br>
      <i>value</i> has to be a numeric value. Values less than 30 will be interpreted as 0 because the Yeelight's lower limit is 30.
      </li>
    <li>disable<br>
      1 -> disconnect from the bulb</li>
  </ul>
</ul>
=end html
=cut
